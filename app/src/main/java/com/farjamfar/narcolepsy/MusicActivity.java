package com.farjamfar.narcolepsy;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MusicActivity extends AppCompatActivity {

    private ImageView imgView;
    private RelativeLayout musicButton;
    private MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        this.imgView = findViewById(R.id.music_image);
        this.musicButton = findViewById(R.id.play_music);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int result = extras.getInt("result");
            if (result >= 3) {
                imgView.setImageResource(R.drawable.pic_positive);
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.positive);
            } else if (result < 3) {
                imgView.setImageResource(R.drawable.pic_negative);
                mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.negative);
            }
        }

        musicButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.stop();
        mediaPlayer.release();
    }
}