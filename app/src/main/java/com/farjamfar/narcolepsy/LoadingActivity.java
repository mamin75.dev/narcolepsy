package com.farjamfar.narcolepsy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class LoadingActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        SharedPreferences pref = getSharedPreferences(RegisterActivity.sharedPrefName, MODE_PRIVATE);
        String email = pref.getString("emailAddress", "");


        if (email.isEmpty()) {
            startActivity(new Intent(LoadingActivity.this, WelcomeActivity.class));
        } else {
            startActivity(new Intent(LoadingActivity.this, QuestionsActivity.class));
        }
    }
}