package com.farjamfar.narcolepsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class QuestionsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView nameTV;
    private ImageView exerciseLike, exerciseDisLike,
        dietLike, dietDisLike,
        bedtimeLike, bedtimeDisLike,
        meditationLike, meditationDisLike,
        caffeineAlcoholLike, caffeineAlcoholDisLike;
    private AppCompatButton submitButton;
    private int exercise, diet, bedtime, meditation, caffeineAlcohol;

    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        initViews();
        this.exerciseLike.setOnClickListener(this);
        this.exerciseDisLike.setOnClickListener(this);
        this.dietLike.setOnClickListener(this);
        this.dietDisLike.setOnClickListener(this);
        this.bedtimeLike.setOnClickListener(this);
        this.bedtimeDisLike.setOnClickListener(this);
        this.meditationLike.setOnClickListener(this);
        this.meditationDisLike.setOnClickListener(this);
        this.caffeineAlcoholLike.setOnClickListener(this);
        this.caffeineAlcoholDisLike.setOnClickListener(this);

        this.submitButton.setOnClickListener(this);

        SharedPreferences preferences = getSharedPreferences(RegisterActivity.sharedPrefName, MODE_PRIVATE);
        nameTV.setText("Hello, " + preferences.getString("name", ""));
    }

    private void initViews() {
        this.nameTV = findViewById(R.id.name_header);

        this.exerciseLike = findViewById(R.id.exercise_like);
        this.exerciseDisLike = findViewById(R.id.exercise_dislike);
        this.dietLike = findViewById(R.id.diet_like);
        this.dietDisLike = findViewById(R.id.diet_dislike);
        this.bedtimeLike = findViewById(R.id.bedtime_like);
        this.bedtimeDisLike = findViewById(R.id.bedtime_dislike);
        this.meditationLike = findViewById(R.id.meditation_like);
        this.meditationDisLike = findViewById(R.id.meditation_dislike);
        this.caffeineAlcoholLike = findViewById(R.id.caffeine_alcohol_like);
        this.caffeineAlcoholDisLike = findViewById(R.id.caffeine_alcohol_dislike);

        this.submitButton = findViewById(R.id.question_submit_button);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exercise_like: {
                this.exercise = 1;
                this.exerciseLike.setImageResource(R.drawable.sentiment_satisfied_alt_green);
                this.exerciseDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24_black);
                break;
            }
            case R.id.exercise_dislike: {
                this.exercise = 0;
                this.exerciseDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24);
                this.exerciseLike.setImageResource(R.drawable.sentiment_satisfied_black);
                break;
            }
            case R.id.diet_like: {
                this.diet = 1;
                this.dietLike.setImageResource(R.drawable.sentiment_satisfied_alt_green);
                this.dietDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24_black);
                break;
            }
            case R.id.diet_dislike: {
                this.diet = 0;
                this.dietDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24);
                this.dietLike.setImageResource(R.drawable.sentiment_satisfied_black);
                break;
            }
            case R.id.bedtime_like: {
                this.bedtime = 1;
                this.bedtimeLike.setImageResource(R.drawable.sentiment_satisfied_alt_green);
                this.bedtimeDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24_black);
                break;
            }
            case R.id.bedtime_dislike: {
                this.bedtime = 0;
                this.bedtimeDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24);
                this.bedtimeLike.setImageResource(R.drawable.sentiment_satisfied_black);
                break;
            }
            case R.id.meditation_like: {
                this.meditation = 1;
                this.meditationLike.setImageResource(R.drawable.sentiment_satisfied_alt_green);
                this.meditationDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24_black);
                break;
            }
            case R.id.meditation_dislike: {
                this.meditation = 0;
                this.meditationDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24);
                this.meditationLike.setImageResource(R.drawable.sentiment_satisfied_black);
                break;
            }
            case R.id.caffeine_alcohol_like: {
                this.caffeineAlcohol = 1;
                this.caffeineAlcoholLike.setImageResource(R.drawable.sentiment_satisfied_alt_green);
                this.caffeineAlcoholDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24_black);
                break;
            }
            case R.id.caffeine_alcohol_dislike: {
                this.caffeineAlcohol = 0;
                this.caffeineAlcoholDisLike.setImageResource(R.drawable.ic_baseline_sentiment_very_dissatisfied_24);
                this.caffeineAlcoholLike.setImageResource(R.drawable.sentiment_satisfied_black);
                break;
            }
            case R.id.question_submit_button: {
                int result = this.exercise + this.diet + this.bedtime + this.meditation + this.caffeineAlcohol;

                preferences = getSharedPreferences(RegisterActivity.sharedPrefName, MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String currentDateTimeString = formatter.format(new Date());

                editor.putInt("result" + currentDateTimeString, result);
                editor.commit();

                String weeklyResult = checkPreviousWeekResult();
                Intent intent;
                if (weeklyResult == "Emergency") {
                    intent = new Intent(QuestionsActivity.this, EmergencyActivity.class);
                } else {
                    intent = new Intent(QuestionsActivity.this, MusicActivity.class);
                    intent.putExtra("result", result);
                }

                startActivity(intent);
                break;
            }
        }
    }

    private String checkPreviousWeekResult() {
        ArrayList<Integer> pointList = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -i);
            int dateRes = preferences.getInt("result" + dateFormat.format(cal.getTime()), -1);
            if (dateRes > -1) {
                pointList.add(dateRes);
            }
        }
        int counter = 0;
        for (int j = 0; j < pointList.size(); j++) {
            if (pointList.get(j) < 3) {
                counter++;
            }
        }
        if (counter >= 3) {
            for (int i = 1; i <= 3; i++) {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -i);
                SharedPreferences.Editor editor = preferences.edit();
                editor.remove("result" + dateFormat.format(cal.getTime()));
                editor.commit();
            }
            return "Emergency";
        }
        return "Normal";
    }
}