package com.farjamfar.narcolepsy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class EmergencyActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView callButton, mailButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency);

        initViews();

        this.callButton.setOnClickListener(this);
        this.mailButton.setOnClickListener(this);
    }

    private void initViews() {
        this.callButton = findViewById(R.id.call_button);
        this.mailButton = findViewById(R.id.mail_button);
    }

    @Override
    public void onClick(View view) {
        SharedPreferences preferences = getSharedPreferences(RegisterActivity.sharedPrefName, MODE_PRIVATE);
        if (view.getId() == R.id.call_button) {
            String phone = preferences.getString("telSupporter", "");
            if (phone.length() > 0) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                startActivity(intent);
            }
        } else if (view.getId() == R.id.mail_button) {
            String mail = preferences.getString("emailAddressSupporter", "");
            if (mail.length() > 0) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { mail });
                startActivity(Intent.createChooser(intent, ""));
            }
        }


    }
}