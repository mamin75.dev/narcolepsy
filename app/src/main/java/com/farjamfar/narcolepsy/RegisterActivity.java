package com.farjamfar.narcolepsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView nameTV, ageTV, emailTV, emailSupporterTV, telTV, telSupporterTV;
    private AppCompatButton submitButton;
    public static String sharedPrefName = "MySharedPref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
        submitButton.setOnClickListener(this);
    }

    private void initViews() {
        this.nameTV = findViewById(R.id.name_input);
        this.ageTV = findViewById(R.id.age_input);
        this.emailTV = findViewById(R.id.email_input);
        this.emailSupporterTV = findViewById(R.id.email_supporter_input);
        this.telTV = findViewById(R.id.tel_input);
        this.telSupporterTV = findViewById(R.id.tel_supporter_input);
        this.submitButton = findViewById(R.id.form_submit_button);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.form_submit_button) {
            SharedPreferences preferences = getSharedPreferences(sharedPrefName, MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            if (validateForm()) {
                editor.putString("name", this.nameTV.getText().toString().trim());
                editor.putInt("age", Integer.parseInt(this.ageTV.getText().toString().trim()));
                editor.putString("emailAddress", this.emailTV.getText().toString().trim());
                editor.putString("emailAddressSupporter", this.emailSupporterTV.getText().toString().trim());
                editor.putString("tel", this.telTV.getText().toString().trim());
                editor.putString("telSupporter", this.telSupporterTV.getText().toString().trim());

                editor.commit();

                startActivity(new Intent(RegisterActivity.this, QuestionsActivity.class));
            }



        }
    }

    private boolean validateForm() {
        String name = this.nameTV.getText().toString().trim();
        String age = this.ageTV.getText().toString().trim();
        String email = this.emailTV.getText().toString().trim();
        String emailSupporter = this.emailSupporterTV.getText().toString().trim();
        String tel = this.telTV.getText().toString().trim();
        String telSupporter = this.telSupporterTV.getText().toString().trim();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (name.isEmpty()) {
            Toast.makeText(this, "name is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (age.isEmpty()) {
            Toast.makeText(this, "age is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (email.isEmpty() || !email.matches(emailPattern)) {
            Toast.makeText(this, "invalid email", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (emailSupporter.isEmpty() || !emailSupporter.matches(emailPattern)) {
            Toast.makeText(this, "invalid email supporter", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (tel.isEmpty()) {
            Toast.makeText(this, "tel is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (telSupporter.isEmpty()) {
            Toast.makeText(this, "tel supporter is required", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}