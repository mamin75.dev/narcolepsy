package com.farjamfar.narcolepsy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private AppCompatButton registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        initViews();
        this.registerButton.setOnClickListener(this);
    }

    private void initViews() {
        this.registerButton = findViewById(R.id.register_btn);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.register_btn) {
            startActivity(new Intent(WelcomeActivity.this, RegisterActivity.class));
        }
    }
}